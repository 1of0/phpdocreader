<?php

namespace OneOfZero\PhpDocReader\Tests\FixturesReturnTag;

use OneOfZero\PhpDocReader\Tests\FixturesReturnTag\DependencyClass1 as Foo;
use OneOfZero\PhpDocReader\Tests\FixturesReturnTag\DependencyClass2 as Bar;

class Class1
{
    /**
     * @return Foo
     */
    public function singleReturnType()
    {

    }

    /**
     * @return Foo|Bar
     */
    public function multipleReturnType()
    {

    }
}
