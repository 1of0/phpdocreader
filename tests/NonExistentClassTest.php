<?php

namespace OneOfZero\PhpDocReader\Tests;

use OneOfZero\PhpDocReader\PhpDocReader;
use ReflectionParameter;

/**
 * Test exceptions when a class doesn't exist.
 */
class NonExistentClassTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException \OneOfZero\PhpDocReader\AnnotationException
     * @expectedExceptionMessage The @var annotation on OneOfZero\PhpDocReader\Tests\FixturesNonExistentClass\Class1::prop contains a non existent class "Foo". Did you maybe forget to add a "use" statement for this annotation?
     */
    public function testProperties()
    {
        $parser = new PhpDocReader();
        $class = new \ReflectionClass('OneOfZero\PhpDocReader\Tests\FixturesNonExistentClass\Class1');

        $parser->getPropertyClass($class->getProperty('prop'));
    }

    /**
     * @expectedException \OneOfZero\PhpDocReader\AnnotationException
     * @expectedExceptionMessage The @param annotation for parameter "param" of OneOfZero\PhpDocReader\Tests\FixturesNonExistentClass\Class1::foo contains a non existent class "Foo". Did you maybe forget to add a "use" statement for this annotation?
     */
    public function testMethodParameters()
    {
        $parser = new PhpDocReader();
        $parameter = new ReflectionParameter(array('OneOfZero\PhpDocReader\Tests\FixturesNonExistentClass\Class1', 'foo'), 'param');

        $parser->getParameterClass($parameter);
    }
}
