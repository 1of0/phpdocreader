<?php

namespace OneOfZero\PhpDocReader\Tests\FixturesIssue335;

use OneOfZero\PhpDocReader\Tests\FixturesIssue335\ClassX as Bar;
use OneOfZero\PhpDocReader\Tests\FixturesIssue335\ClassX as MethodBar;

trait Trait2
{
    /**
     * @var Bar $propTrait2
     */
    protected $propTrait2;
    
    /**
     * @param MethodBar $parameter
     */
    public function methodTrait2($parameter)
    {

    }
}
