<?php

namespace OneOfZero\PhpDocReader\Tests\FixturesIssue335;

use OneOfZero\PhpDocReader\Tests\FixturesIssue335\ClassX as Foo;
use OneOfZero\PhpDocReader\Tests\FixturesIssue335\ClassX as MethodFoo;

trait Trait1
{
    /**
     * @var Foo $propTrait1
     */
    protected $propTrait1;

    /**
     * @param MethodFoo $parameter
     */
    public function methodTrait1($parameter)
    {
        
    }
}
