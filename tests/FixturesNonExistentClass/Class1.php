<?php

namespace OneOfZero\PhpDocReader\Tests\FixturesNonExistentClass;

class Class1
{
    /**
     * @var Foo
     */
    public $prop;

    /**
     * @param Foo $param
     */
    public function foo($param)
    {
    }
} 
